const app = require('@nomondaypl/front-core').app;
app.set('port', process.env.PORT || 3000);
const server = app.listen(app.get('port'), () => {
    console.log(`Aplikacja nasłuchuje na porcie ${ server.address().port }`);
});