# NoMonday Front-Starter
=======================

## Zastrzeżenia
---------------------------
To repozytorium jest własnocią NoMonday Sp. z o.o. i jako takie podlega ochronie. Nieautoryzowane korzystanie, udostępnianie, aktualizacja i tym podobne jest ściśle zakazane. Każdy przypadek naruszenia będzie ścigany z całą stanowczością.
Kontakt: hello@nomonday.pl


## Wprowadzenie
---------------------------
Startowa aplikacja Front-end.  
Kontakt deweloperski: wojciech.gorzawski@nomonday.pl

## Instalacja
---------------------------
1. Pobrać aplikację z brancha `master`
1. Zainstalować Nodemon `npm install -g nodemon`
2. Zainstalować zależności  NODE'a za pomocą `npm install`

## Używanie
---------------------------
Komendy CLI:

1. Odpalenie aplikacji - `nodemon`
2. Odpalenie Gulp'a - `gulp default`
3. Wylistowanie Gulpowych taksów - `gulp --tasks`
4. Generowanie statycznej aplikacji - `node builder`